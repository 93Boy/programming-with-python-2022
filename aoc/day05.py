
with open('day05.txt') as f:
    data = f.readlines()

first = ['Z','J','G']
second = ['Q','L','R','P','W','F','V','C']
third = ['F','P','M','C','L','G','R']
fourth = ['L','F','B','W','P','H','M']
fifth = ['G','C','F','S','V','Q']
sixth = ['W','H','J','Z','M','Q','T','L']
seventh = ['H','F','S','B','V']
eighth = ['F','J','Z','S']
nineth = ['M','C','D','P','F','H','B','T']

val_dic = {1 : 'first', 2 : 'second', 3 : 'third', 4 : 'fourth' , 5 : 'fifth', 6 : 'sixth', 7: 'seventh', 8: 'eighth', 9: 'nineth'}

counter = 0
for line in data:
    if 'move' in line[0:4]:
        num = int(line[5:7].strip())
        to = int(line[17:19].strip())
        From = int(line[12:14].strip())
        #print(f'move {num} from {From} to {to}')
        x = locals()[val_dic.get(to)]
        y = locals()[val_dic.get(From)]
        rm = y[-num:]
        #print(rm)
        for i in rm: #for part one just iterate in revered order.for i in reversed(rm)
            x.append(i)
        print(x)
        del y[-num:]
        print(y)


#part 2 output
for a in val_dic.keys():
    b = locals()[val_dic.get(a)]
    print(b[-1])



