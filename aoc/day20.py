import copy
from collections import deque

def sol(copy_data): #get the deque of list of tuples as input(index,item)
  for i in range(len(copy_data)): #iterating upto the length of data
      while True:
          if i == copy_data[0][0]: #copy_data[0][0] gives the original index of data
              break #if 2 indexes are equal break.
          else: #if not rotate one in anti clockwise. imagine data is on a circle.
              copy_data.rotate(-1) #rotate 1 left side means 0 index will goto last index. 1st index will be zero index

      item = copy_data.popleft() #taking out 0 index object
      if not item[1] == 0: #if item is not zero
          copy_data.rotate(-item[1]) #copy_data deque need to rotate leftside amount of times equal to items numerical value
          #used -item[1] to make sure if item is positive then it will move to left. if item is negative (-) * (-) is positbe so item will be moved to right
          copy_data.appendleft(item) #then that removed item appended back to left end
      else:
          copy_data.appendleft(item)
#by this step all data will be sorted according to parameters of problem description
  ite = [k[1] for k in copy_data] #extracting only the item values of deque
  ans = 0
  for i in [1000, 2000, 3000]: #given coordinates
      ans += ite[(ite.index(0) + i) % len(ite)] #ite.index(0) gives with index of value zero. %len(ite) give how many times it need to
      #len(ite) defines the number of positions to make one rotation.
      #reminder of (ite.index(0) + i) % len(ite) gives the reminder afterlooping

  return ans


with open("day20.txt") as f:
    data = f.readlines() #reading data

d_list1 = [(ind,int(i)) for ind,i in enumerate(data)] #making a list of tuple with (index of input data,input data)
#this index is very important since question say we need to manipulate the data without changing its original order
#this index help us to keep a track of orginal order of data
copy_data1 = deque(copy.deepcopy(d_list1))
#make a deep copy of data.so it wont edit original. used deque since it lets to push nd pop from both ends
print('P1 ans is',sol(copy_data1))

#for part 2 just aplly given encryption key and mix the numbers for 10 times
d_list2 = [(ind,int(i)*811589153) for ind,i in enumerate(data)] #key added
copy_data2 = deque(copy.deepcopy(d_list2))

for i in range(10):
    P2 = sol(copy_data2)


print('P2 ans is',P2)
