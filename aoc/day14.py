"""""
  4     5  5
  9     0  0
  4     0  3
0 ......+...     (x=500,y=0)
1 ..........     y only increasing downwards
2 ..........     x can go both left nd right
3 ..........
4 ....#...##
5 ....#...#.
6 ..###...#.
7 ........#.
8 ........#.
9 #########.  abyss

"""
with open('day14.txt') as f:
    data = f.read().strip().split("\n")
v = set() #grid points of rocks
for i in data:
    coordinates = [tuple(map(int, cord.split(","))) for cord in i.split(" -> ")]
    for S,E in zip(coordinates,coordinates[1:]):
        #S,E gives tuples.
        x1,y1 = S #previous x,y coordinates
        x2,y2 = E #current x,y coordinates
        if x1 == x2 :
            for y in range(min(y1,y2 ), max(y1, y2) + 1): #horizontal line.x coords. min,max functions used values not always incrementing.we dunno which is smaller to define range
                v.add((x2,y))
        if y1 == y2 :
            for x in range(min(x1,x2 ), max(x1, x2) + 1): #verticle line. going down with y coords
                v.add((x,y2))
#print(v) gives a set of tuples of coords in (x,y) format
#abyss is the last line of rocks. Sands cannot go below this line.its the maximum level of y coordinates.
abyss = max([i[1] for i in v])

#simulate the cave. has to follow the falling sand.
dropped_sand = 0

directions = [(0, 1), (-1, 1), (1, 1)] #directions how a sand particle can move with (x,y) coordinates
#(0,1) = (x,y+1) fall down straight(y+1).no side(x) moment.condition 1 , (-1,1) = (x-1,y+1) fall down and move left.condition 2 ,(1,1) (x+1,y+1). fall down and move right.condition 3

while True:
    x, y = 500, 0 #initial coordinates of sand release point
    while True:
        if y >= abyss or (500, 0) in v: #if y cord reached to abyss or if 500,0 in v means it filled to the mouth
            print("part 1 ans is",dropped_sand)
            exit(0)
        for i, j in directions:    #checking wethr sand can move
            nx, ny = x + i, y + j #redefining new x and y values as nx,ny
            if (nx, ny) not in v: #checking wthr they are in v.
                break        #exit the if loop
        else:
            v.add((x, y)) #if those values arent in v, update v. it means this sand now stopped moving.
            dropped_sand += 1 #increase sand count
            break
        x, y = nx, ny #update x,y values to continue the loop

#Part 2###
#Outer while loop,
# Inner while loop to check to check the status of sand particles and control its moment through if loops.an alternative to the for loop of part 1.
#2 Inner for loops to check the momemnt of particle coordinate fullfil the exit requirments. without this it will loop for an infinity

new_sand = 0

while True:
    x, y = 500, 0

    while y <= abyss:
        if (x, y + 1) not in v:
            y += 1
            continue

        if (x - 1, y + 1) not in v:
            x -= 1
            y += 1
            continue

        if (x + 1, y + 1) not in v:
            x += 1
            y += 1
            continue

        break
    if (x, y) in v:
       break

    v.add((x, y))
    new_sand += 1

    if (x, y) == (500, 0):
        break

print("part 2 ans",new_sand)

