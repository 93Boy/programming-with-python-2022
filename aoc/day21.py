
monkeys = {} #{monkey name: number ,scwf: 4 }
with open("day21.txt") as file:
    lines = [line.strip() for line in file]

def evaluate_expression(left, op, right, monkeys):
    left_value = monkeys[left] if left in monkeys else left #check wthrleft monkey name in monkey dic.else it sets left_value to left itself
    right_value = monkeys[right] if right in monkeys else right #same as left
    return eval("{} {} {}".format(left_value, op, right_value)) #use eval function to reformat left , op and right as a python expression

for i in lines:
    n, eq = i.split(": ") #splitting monkey name and expression. tmgd: zrwl - qqtv
    if eq.isdigit(): #checking if expression is a number.
        # if so its assigned under monekys name
        monkeys[n] = int(eq)
    else:
        l, op, r = eq.split() #checking expression part into left , operation , right
        if l in monkeys and r in monkeys: #cheking wether left and right present in monkeys
            monkeys[n] = evaluate_expression(l, op, r, monkeys) #calling function. function returns a numeric answer
        else:
            lines.append(i) #If the left and right operands are not both in the monkeys dictionary,
            # it appends the line back to the end of the lines list.


print(monkeys["root"])

##part 2
