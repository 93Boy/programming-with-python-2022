
with open('day04.txt') as f:
    data = f.readlines()
count = 0
com_count = 0
for line in data:
    first , second = line.split(',')
    first1 , first2 = first.split('-')
    second1 , second2 = second.split('-')
    firstelve = [i for i in range(int(first1) ,int(first2)+1)]
    secondelve = [k for k in range(int(second1), int(second2) + 1)]
    if all(x in firstelve for x in secondelve) is True:
        count = count+1
    elif all(y in secondelve for y in firstelve) is True:
        count = count+1
    common_ele = set(firstelve).intersection(secondelve)
    if len(common_ele) != 0 :
        com_count = com_count +1

print(com_count)
