with open("day10.txt") as f:
    data = f.read().strip()

code = [x for x in data.split('\n')] #getting instructions parsed


x = 1  #reg val
cnt = 0 #counter
flags = [20, 60, 100, 140, 180, 220]
strength = 0

for i in code:
    ins = i.split(" ",1) #maxsplit=1 gives list of 2 elements
    if ins[0] == 'noop':
        cnt += 1
        if cnt in flags:
            strength += cnt*x
    elif ins[0] == 'addx':
        cnt += 1 #increment counter for 1st time of addx 2 steps
        if cnt in flags:
            strength += cnt * x
        cnt += 1 #increment counter for 2nd time
        if cnt in flags:
            strength += cnt * x
        val = int(ins[1])
        x += val

print("Ans for P1 is",strength)

#######Part 2#####

pattern = [['n/a' for _ in range(40)]  for _ in range(6)] #creating empty list
#iterate through cycle time ct instead of counter(cnt)
x = 1
cycles = [1]
for i in code:
    ins = i.split(" ",1)
    if ins[0] == 'noop':
        cycles.append(x)
    elif ins[0] == 'addx':
        cycles.append(x) #first cycle
        x += int(ins[1]) #update x
        cycles.append(x) #2nd cycle
#could take above section if 1 function and use for both part 1 and 2.Then need to have another check for loop

for R in range(6): #ietrate through rows
    for C in range(40): #cols. left most pixel is 0 as per description
        k = R*40 + C + 1 #R=0 C=0 is cycle 1. K is current cycle time
        if abs(cycles[k - 1] - C) <= 1: #cycles gives the lcation of sprites. if its 1 away from current column draw ##
            pattern[R][C] = "###" #one # is not clear
        else:
            pattern[R][C] = "   "

for i in pattern:       #printing
    print("".join(i))



