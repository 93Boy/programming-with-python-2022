from collections import defaultdict

with open('day07.txt') as f:
    data = f.read().strip().split('\n') #getting data to a list

dict_sz = defaultdict(int) #to prevent unnecessary key errors
path = []

for i in data:
    coms = i.strip().split()
    if coms[1] == 'cd':
        if coms[2] == '..':
            path.pop()
        else:
            path.append(coms[2])
    elif coms[1] == 'ls':
        continue
    elif coms[0] == 'dir':
        continue
    else:
        s = int(coms[0])
        for i in range(1, len(path)+1):
            dict_sz['/'.join(path[:i])] += s

V1 = 0
for v in dict_sz.values():
    if v <=100000:
        V1 += v
#Part 1
print("Total sixe of dir is",V1)

Size_needed = 30000000 - (70000000 - dict_sz["/"]) #dict_sz["/"] is the space used by dirs
needed = []
for v in dict_sz.values():
    if Size_needed <= v: #checking wthr dir values are larger than size needed
        needed.append(v)

print(needed)
print("Smallest dir to delete",min(needed)) #selecting smallest
