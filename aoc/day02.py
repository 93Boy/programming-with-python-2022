
with open('day02.txt') as f:
    data = f.readlines()

A_list = []
B_list = []

for line in data:
    A_list.append(line[0].upper())
    B_list.append(line[2].upper())

vals = {'A' : 'X', 'B' : 'Y' , 'C' : 'Z'}


New_A = []
for i in A_list :
    New_A.append(vals[i])

score_guide = {'X': 1 , 'Y' : 2 , 'Z': 3}
marks = []
for index,ele in enumerate(B_list):
    if ele == New_A[index] :
        print("Tie at location",index)
        marks.append(score_guide.get(ele))
        marks.append(3)
    elif ele == 'X':
        if New_A[index] == 'Z' :
            print("Rock smashes scissors! You win!")
            marks.append(score_guide.get(ele))
            marks.append(6)
        else :
            print("Paper covers rock! You lose.")
            marks.append(score_guide.get(ele))
    elif ele == 'Y':
        if New_A[index] == 'X' :
            print("Paper covers rock! You win!")
            marks.append(score_guide.get(ele))
            marks.append(6)
        else :
            print("Scissors cuts paper! You lose.")
            marks.append(score_guide.get(ele))
    elif ele == 'Z':
        if New_A[index] == 'Y':
            print("Scissors cuts paper! You win!")
            marks.append(score_guide.get(ele))
            marks.append(6)
        else:
            print("Rock smashes scissors! You lose.")
            marks.append(score_guide.get(ele))


print(sum(marks))

#part2
marks = []
score_guide = {'A': 1 , 'B' : 2 , 'C': 3}
for index , ele in enumerate(B_list):
    if ele == 'Y':
        print('Tie at',index)
        marks.append(3)
        marks.append(score_guide.get(A_list[index]))
    elif ele == 'X':
        print('loose at',index)
        if A_list[index] == 'A':     #rock
            marks.append(3)          #scissor
        elif A_list[index] == 'B':   #paper
            marks.append(1)          #rock
        else :                       #scissor
            marks.append(2)
    elif ele == 'Z':
        print('Won at',index)
        marks.append(6)
        if A_list[index] == 'A':        #rock
            marks.append(2)
        elif A_list[index] == 'B':      #paper
            marks.append(3)
        else :                          #scissor
            marks.append(1)


print(marks)






print(sum(marks))








