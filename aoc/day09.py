
h = [0,0] #initializing head nd tail
t = [0,0]

with open('day09.txt') as f :
    data = f.read().strip().split("\n")

directions = {'L': [-1, 0], 'R': [1, 0], 'D': [0, -1], 'U': [0, 1]} #diection list

moves = [(directions[line[0]], int(line[1:])) for line in data] #mapping direction info to moves


t_visited = set()

for i in moves:
    for _ in range(i[1]):
        h[0] += i[0][0]  #x coordinate. Adding the relevent x coordinate from move to the head part.
        h[1] += i[0][1]  #y coordinate. Adding the relevent y coordinate
        x = h[0] - t[0]  #checking the distence differecne of x cords of head and tail
        #if h-t is negative tail moves to left. if its + tail moves right. 0 if its in same colum
        y = h[1] - t[1]  #distence difference of y cords

        if abs(x) > 1 or abs(y) > 1: #tail should move if either of the difference in x and y are 2 or more
            if x == 0: t[1] += y // 2 #if x is equal to 0 they in same colmn.we only need to change verticle.
            # the y value of tail need to change in the direction of 'y' variable. need to divide by 2 since y value is 2

            elif y == 0: t[0] += 1 if x > 0 else -1  #same logic as above. x = o means both in same row.
            # use floor division since x can be either +2 or -2. similar to t[0] += 1 if x > 0 else -1
            else: #if neighther is 0. then has to move diagonaly
                t[0] += 1 if x > 0 else -1 #cant use floor division as above becuase if x can range from -2 to +2
                t[1] += 1 if y > 0 else -1

        t_visited.add(tuple(t)) #sets dosent support lists. python lists use hashing but its not supported in sets
        #tuples are immutable so can add to sets

print('P1 ans is',len(t_visited))

##Part 2###

K = [[0, 0] for _ in range(10)] #got 10 knots here instead of head n tail
visited = set()
for i in moves:
    for _ in range(i[1]):
        K[0][0] += i[0][0]  #x coordinate. Adding the relevent x coordinate from move to the head part.
        K[0][1] += i[0][1]  #y coordinate. Adding the relevent y coordinate

        for x in range(9): #as per description
           head = K[x]   #front knot
           tail = K[x + 1] #part 2 tail dosnt move. every knot follows its previous knots moment
           x = head[0] - tail[0]
           y = head[1] - tail[1]
           if abs(x) > 1 or abs(y) > 1: #tail should move if either of the difference in x and y are 2 or more
              if x == 0: tail[1] += y // 2 #if x is equal to 0 they in same colmn.we only need to change verticle.
              # the y value of tail need to change in the direction of 'y' variable. need to divide by 2 since y value is 2

              elif y == 0: tail[0] += 1 if x > 0 else -1  #same logic as above. x = o means both in same row.
              # use floor division since x can be either +2 or -2. similar to t[0] += 1 if x > 0 else -1
              else: #if neighther is 0. then has to move diagonaly
                  tail[0] += 1 if x > 0 else -1 #cant use floor division as above becuase if x can range from -2 to +2
                  tail[1] += 1 if y > 0 else -1
                  #print(tail[0],tail[1])
           visited.add(tuple(K[-1]))




print(len(visited))













