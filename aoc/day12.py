from collections import deque

with open("day12.txt") as f:
     grid = [list(line) for line in f.read().strip().split()]

n = len(grid)       #row
m = len(grid[0])    #col
q = deque()   #making a deque object

for i, row in enumerate(grid): #itter through coordinates
    for j, item in enumerate(row):
        if item == "S":
            str = i
            stc = j
            grid[i][j] = "a"
        if item == "E":
            endr = i
            endc = j
            grid[i][j] = "z"

q.append((0,str,stc)) #manualy adding starting coordinates and distence to get that point. initially 0. adding as a tuple as deque takes only 1 argument a time
visited = {(str,stc)} #making visited nod set so the loop dosent count visited nodes

while len(q) != 0:
    steps, i, j = q.popleft() #getting out left most elements
    #print(steps,i,j)
    for ii, jj in [(i + 1, j), (i - 1, j), (i, j + 1), (i, j - 1)]: #itter through possible neibhour cells. one row up , 1 R down , 1 col right , 1 col left
        if ii < 0 or jj < 0 or ii >= n or jj >= m : #checking wethr current row nd col (ii,jj) is valid.
            continue
        if ii == endr and jj == endc: #if current row nd col is matching to coordinates of "E"
            print('step count is',steps+1) #print step count and break the loop
            exit(0)
        if (ii, jj) in visited: #if coordinates in visited pass
            continue
        if ord(grid[ii][jj]) - ord(grid[i][j]) > 1: #get the elevation different to the target.ord() is used to cast unicode char into a numerical val
            continue
#this take one coordinate set and compare it with elements in x,y axis. if a match is not found it appends value set into deque so next time it will be inputs
        visited.add((ii, jj)) #add coordinates to visited set
        q.append((steps + 1, ii, jj)) #append current infor to continue the loop

####Part 2####
#start from end corrdinates and stop at nearest a.
#this time direction of neibhours are different.

q2 = deque()
q2.append((0,endr,endc))  #sstarting with "E" and look for closest "a"
visited2 = {(endr, endc)}

while q2:
    steps, i, j = q2.popleft()
    for ii, jj in [(i + 1, j), (i - 1, j), (i, j + 1), (i, j - 1)]:
        if ii < 0 or jj < 0 or ii >= n or jj >= m:
            continue
        if (ii, jj) in visited2:
            continue
        if ord(grid[ii][jj]) - ord(grid[i][j]) < -1: #grid difference should be smaller than -1 since we go downhill
            continue
        if grid[ii][jj] == "a":  #check wthr current grid is equal to "a"
            print("Part2 is",steps + 1)
            exit(0)
        visited2.add((ii, jj))
        q2.append((steps + 1, ii, jj))