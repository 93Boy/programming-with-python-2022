
with open('day06.txt') as f:
    data = f.read()             #take whole as a string
k = 14                          # K is 4 for part 1. 14 for part 2
for a in range (len(data)-k+1): #used moving average method
    win = data[a : a + k]
    if len(win.strip()) == 14 :
        x = set(win)
        if len(win) == len(x):
            print("4 uniques characters at",a+k)


#duplicate finding can be done with list comprehensions [x for n, x in enumerate(win) if x not in win[:n]]






