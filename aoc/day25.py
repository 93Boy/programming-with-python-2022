
global digits
digits = "=-012"
def de2b5(decimal):
    output = ""
    while decimal:
        rem = decimal % 5 #number mod 5 and get reminder
        decimal = decimal // 5  #floor division by 5 to get its next digit in base 5 reprentation.
        if rem <= 2: #If remainder is less than or equal to 2
            # it means that the relevent base-5 character should be either 0, 1, or 2. In this case, we add the string representation of remainder to the start of output.
            output = str(rem) + output
        else: #if rem is > 2 it means , relevent base 5 character is either - or =
            output = "   =-"[rem] + output #" =-"[remainder] is used to get relevent base 5 character. " =-"[0] return 0
            decimal += 1 #decimal value incremented
    return output


with open("day25.txt") as f:
    data = f.read().splitlines() #parse input

total = 0

for line in data:
    coef = 1 #coefficient variable.
    # Calculate the decimal value of each line
    for x in line[::-1]: #reading each line in reversed order
        dec_val = digits.find(x) - 2 #-2 is becuase this base 5 start from -2. regular it should be 0
        total += dec_val * coef
        coef *= 5 #multiply by 5 to ensure each base 5 position to keep up with its decimal equilent.
        #1st position x*1 , 2nd x*5 , 3rd x*25...

# Convert total to base5 representation
print(de2b5(total))
