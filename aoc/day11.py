
with open("day11.txt") as f:
    data = f.read().strip().split("\n\n")

monkeys = []

for i in data:
    cont = i.splitlines()
    dummy = [] #dummy that old each passed monkey information
    items = list(map(int, cont[1].split(":")[1].strip().split(","))) #passing item list. seperate vals using split() and use map method to pack it in a list form
    dummy.append(items)
    op = cont[2][2:].split(" ", 3)[3].split(" ") #passing operation info [old,op,nw_val] format
    dummy.append(op)
    for j in cont[3:]:
        dummy.append(int(j.split()[-1]))
    monkeys.append(dummy)
#structure of monkeys : [[items] , [old,sign,new_val] , div_num ,true_val , fales_val]
#monkey[0]=items , monkey[1] = list of ops , monkey[3] = div_num

num = [0]* len(monkeys) #generating a list for nums of monkeys
def sol(ops,v):
#didnt use old value as its always start with old. old value gives with the item v pass. idk it worked
    op = ops[1]
    new = ops [2]
    if op == "+":
        return v + int(new)
    else:
        if new == "old":
            return v * v
        else:
            return v * int(new)



for _ in range(20):
   for index,m in enumerate(monkeys):
       for item in m[0]:
           item = sol(m[1],item) #passing the list of op infor and item numbers to sol method
           item //= 3 #check the worry level. //= is doing floor division. it equals item= item // 3
           if item % m[2] == 0: #doing the test
               monkeys[m[3]][0].append(item) #true option
           else:
               monkeys[m[4]][0].append(item) #test fail option
           num[index] += len(m[0])
           m[0] = []
#print(num)
num.sort()
print('Answer for P1 is',num[-1] * num[-2]) #printing the product of last 2
########Part 2#####
#simplest way is removing item //= 3  and running it for 10000 times instead of 20 but it takes ages
#use moduler maths. idk the math behind it

mod = 1
num = [0]* len(monkeys)

for m in monkeys:
    mod *= m[2] #mod is a multiple of all test factors

for _ in range(10000):
   for index,m in enumerate(monkeys):
       for item in m[0]:
           item = sol(m[1],item) #passing the list of op infor and item numbers to sol method
           item %= mod #modified item variable. %= returns the remainder of dividing the left hand operand by right hand operand.
           if item % m[2] == 0: #doing the test
               monkeys[m[3]][0].append(item) #true option
           else:
               monkeys[m[4]][0].append(item) #test fail option
           num[index] += len(m[0])
           m[0] = []

num.sort()
print('Answer for P2 is',num[-1] * num[-2]) #printing the product of last 2
#has to run part 2 seperately.if not gives a different value





