import re
with open('day15.txt') as f:
    data = f.readlines()

Y=2000000 #Row value where y=2000000
becons = set() #all the known beacons
l = set()

for i in data:
  sx, sy, bx, by = map(int, re.findall(r"-?\d+", i)) #parsing input using a regex.

  if by == Y :   #checking wthr becon y cord is 200000. so the beacon is within limit
      becons.add(by)

  dx = (abs(sx - bx) + abs(sy - by)) - abs(sy - Y)
  #abs(bx - sx) + abs(by - sy) is the absolute difference of any 2 points. x axix - abs dif of y axis
  # P1 need to calculate X value where y=2000000, x: thresh_dist => abs(sx-X) + abs(sy - Y)
  #if dx is less than 0 means those coordinates below the range we need.
  if dx < 0 :
      continue #so we need to skip the rest of steps nd go to the next set of coordinates
  #to calculate all the positions that CANNOT be in this y value we need low and high ranges of sensor x cords

  lox = sx-dx #sx-dx is lower x val. left end point
  hix = sx+dx # sx+dx is the upper limit.right end point
  # lox:hix defines the boundary where other becons cant present
  for k in range(lox,hix+1):
      l.add(k)


#Part 1 ans is the positions where a becon cannot present when y=2 million.
#for that we have all the positions between lo end of x and high end of x. becons has numbr of instences y = 2 million
#so part 1 ans we take all impossible positions and reduce once already got a becon
print('P1 ans is',len(l-becons))

####part 2######
#did it seperately to minimize the run time
#part 2 nee to find the missing beacon. so there must be a gap in coverage.need to find that
pars = [list(map(int, re.findall('\-?\d+',i))) for i in data] #parsed data only once if not file will be parsed 4 million times
for r in range(4_000_001): #search through each row. row value is the y cord 
    intervals = [] #range operations consuming alots of time. inorder to minimize run time add coordinates as intervals
    #base of this logic is store all the intervals where the value cannot present.and merge all these intervals to to check overlaping place.
    for sx, sy, bx, by in pars:
        d = (abs(sx - bx) + abs(sy - by)) - abs(sy - r)
        if d < 0 :
            continue
        lox = sx - d
        hix = sx + d   #generating "cannot" data just like ppart 1
        intervals.append((lox,hix)) #storing them
    intervals.sort() #sorting the list to minimize the intersection o coordinates, without them answer is wrong
    conso = [] #the minimal set of intervals that covers the same values
    start, end = intervals[0] #starting cordinates
    for s, e in intervals:
        if end >= s:   #checking for an intersection. check wether the lower bound of current interval is greater than the high bound of previous interval
            end = max(end, e)
        else:
            conso.append((start, end)) #if cannot merge them appending them to
    if (start, end) not in conso: #adding start , end cords if not available
        conso.append((start, end))
    if len(conso) == 1: #check wethr conso lenghth is 1. if so no overlapping freq. so jump to the next iteration of base for loop
        continue #also it prevent the execution oflast calculation until really necessary
    tun_frq = (conso[0][1] + 1) * 4_000_000 + r #tuning frequency as given in problem statement
    break


print("P2 ans is",tun_frq)


















