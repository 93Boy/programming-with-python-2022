import string

with open('day03.txt') as f:
    data = f.readlines()

intersects = []
val = []
for line in data:
    firstpart, secondpart = set(line[:len(line) // 2]), set(line[len(line) // 2:])
    intersects.append(firstpart.intersection(secondpart))

val_list = []

for i in intersects:
    for k in i:
        val_list.append(k)

upper_list = list(string.ascii_uppercase)
lower_list = list(string.ascii_lowercase)
mark_list = []
for v in val_list:
    c = v.islower()
    if c == True:
        mark_list.append(lower_list.index(v)+1)
    else:
        mark_list.append(upper_list.index(v)+27)

print(sum(mark_list))
