from functools import cmp_to_key
with open("day13.txt") as f:
     data = f.read().strip().split('\n\n') #parsing input. this will give an output of 2 sets

Int = lambda i:isinstance(i, int) #making lamda functions for 2 scenarios of lists and ints
List = lambda i:isinstance(i, list)

def compare(l,r): #compare func
    if Int(l) and Int(r): #scenario 1. both are ints
        if l < r:
            return 1
        if l == r :
            return 0
        else:
            return -1
    if Int(l) and List(r): #case 2,3 one is list and other is int. this convert int part also into a list nd return.
        return compare([l],r)
    if List(l) and Int(r): #case 3. same as case 2
        return compare(l,[r])
    if List(l) and List(r): #both are lists.
        for k in range(min(len(l), len(r))): #this for loop works until the minimum val of either of lists. after that no point of executing.
            c = compare(l[k], r[k]) #again calling compare function.
            if c:
                return c
        return compare(len(l), len(r)) #calling compare func.this matches rule number 1 of problem description.

p1 = 0

for index,v in enumerate(data):
    a,b = map(eval,v.split('\n')) #split 2 datasets ny \n and map the eval both inputs. map takes 2 arguments(function,iterable). simillar to apply function in R
    if compare(a,b) == 1: #calling compare fuc for the first time of loop. compare() is nested
        p1 += index + 1
#thr should be a better way as compare method is nested. complications arise when both inputs are list of lists.
#optimal solution can be creating seperate compare methods for lists and its seperatly.
print("part 1 ans is",p1)

#####part 2####
#just need to compare each items against divider keys.
j = list(map(eval, open('day13.txt').read().split())) #parsed input again. if it gives random error in part 1
j += [[[2]], [[6]]] #adding keys elements
j = sorted(j, key=cmp_to_key(compare), reverse=True) #sorted sort any itereble object like list , tuple or dic. it need sorting key
#cmp_to_key was found on reddit. it can override a comparison function.the function we give(compare() in this case) should return an integer value which use to compare values.
#above compare func of p1 gets list or int elements as inputs and returns a val.

print('P2 ans is',(j.index([[2]])+1)*(j.index([[6]])+1)) #adding 1 due to python indexes start with 0
