
with open('day18.txt') as f:
    data = [tuple(map(int, line.split(','))) for line in f.readlines()] #generating input as a list of tuples
    #tuple contains x,y,z cords of rocks
    #have to check each side
sa = 0 #surface area
for x,y,z in data: #iterating cordinates of each rock
    sides = [(1, 0, 0), (-1, 0, 0), (0, 1, 0), (0, -1, 0), (0, 0, 1), (0, 0, -1)]
    #each cube has 6 sides. face side(+) and its oppesite(-)
    for xx,yy,zz in sides: #calculating for each face
        nx , ny ,nz = x+xx , y+yy , z+zz #each face and its face and cords values.
        if (nx , ny , nz) not in data: #if its not connected to another cube it cant have same values as inputs
            sa += 1

print(sa)








