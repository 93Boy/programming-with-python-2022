
with open('day08.txt') as f:
    data = [i.strip() for i in f.readlines()] #creating matrix.

H = len(data[0]) #cols
W = len(data) #rows
#Visible trees. counting boarders
V_trees = (2*W) + (H-2)*2

for i in range(1, W - 1):
    for j in range(1, H - 1):
        tree = data[i][j] #i = row j = col . current indices
        #checking for the maximum values available through X and Y axes of current tree.
        TL = max([data[i][j-k] for k in range (1,j+1)])
        TR = max([data[i][j+k] for k in range (1,H-j)])
        TU = max([data[i-k][j] for k in range(1,i+1)])
        TD = max([data[i+k][j] for k in range(1, W -i)])
        if TL < tree or TR < tree or TU < tree or TD < tree:
            V_trees += 1

print("No. visible trees are",V_trees)

#part 2
with open('day08.txt') as f:
    data = [i.strip() for i in f.readlines()]

H = len(data[0]) #cols
W = len(data) #rows

#initializing scenic score
SC = 0
for i in range(1, W - 1):
    for j in range(1, H - 1):
        tree = data[i][j]
        TL = [data[i][j - k] for k in range(1, j + 1)]
        TR = [data[i][j + k] for k in range(1, H - j)]
        TU = [data[i - k][j] for k in range(1, i + 1)]
        TD = [data[i + k][j] for k in range(1, W - i)]
        CS = 1  # current score. cant set 0 since we multiply
        for a in (TL, TR, TU, TD):
            tmp = 0
            for x in range(len(a)):
                if a[x] < tree:
                    tmp += 1
                elif a[x] >= tree:
                    tmp += 1
                    break

            CS *= tmp
            if CS > SC:
                SC = CS

print("Maxixum scenic score is",SC)